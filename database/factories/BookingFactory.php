<?php

namespace Database\Factories;

use App\Models\Booking;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BookingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Booking::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'firstname' => $this->faker->name,
            'lastname' => $this->faker->lastName,
            'status' => $this->faker->randomElement($array = array('pending', 'approved')),
            'company' => $this->faker->company,
            'telephone' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'date' => Carbon::createFromTimeStamp($this->faker->dateTimeBetween('now', '+7 days')->getTimestamp()),

        ];
    }
}
